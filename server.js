const express = require("express");
const routes = require("./routes");
const logger = require("morgan");
const cors = require("cors");
const dotenv = require("dotenv");
dotenv.config();

const createServer = () => {
  const app = express();
  app.use(cors());
  app.use(logger());
  app.use(express.json());
  app.use("/api", routes);

  return app;
};

module.exports = createServer;
