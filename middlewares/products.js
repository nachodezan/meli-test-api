const responseHandler = require("../utils/responseHandler");

const validateQueryParam = (req, res, next) => {
  const { q } = req.query;
  if (q === undefined || q === "")
    return res.status(402).json(responseHandler.errorResponse(402));
  next();
};

const validateParam = (req, res, next) => {
  const { id } = req.params;
  if (!id) return res.status(402).json(responseHandler.errorResponse(402));
  next();
};

module.exports = { validateParam, validateQueryParam };
