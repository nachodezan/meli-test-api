module.exports = {
  API_URL: "https://api.mercadolibre.com",
  AUTHOR_NAME: "Ignacio",
  AUTHOR_LASTNAME: "De Zan",
  FILTER_CATEGORY_ID: "category",
  PRICE_DECIMALS: 2,
  LIMIT_PRODUCTS: 4,
  ERROR_STATUS_ARRAY: [
    {
      status: "402",
      message: "Mandatory Parameter Empty.",
      data: "Mandatory Parameter Empty.",
    },
    {
      status: "501",
      message: "Data Not Found.",
      data: "Data Not Found.",
    },
  ],
};
