# Rest API Test MercadoLibre

Esta es una API Rest desarrollada en NodeJs para busqueda de productos y descripciones de los mismos, los cuales se obtienen de la plataforma abierta de [MercadoLibre](https://developers.mercadolibre.com.ar/)

## Requerimientos
* NodeJs v14
* npm v7 
* Docker v20.10.2

## Correr el proyecto
Se puede correr via `docker`:

```
docker build -t meli-app .
docker run -it --rm -p 3000:3000 meli-app
```

O desde consola:
```
npm install
npm start
```

## Testing
Para ejecutar testing de la aplicacion:
```
npm test
```
## Librerias
* dotenv
* morgan
* supertest

## Mejoras
* Implementar algun schema validator (ej: `Ajv`).
* Reemplazar `console.error` por libreria de logging (ej: `log4js`, `winston`)
* Usar alguna base de datos en memoria con Redis para mejorar performance

## Agradecimientos
Gracias MercadoLibre por la oportunidad.


