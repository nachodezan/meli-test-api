const { AUTHOR_NAME, AUTHOR_LASTNAME } = require("../config/constants");

const getInfo = () => ({
  name: AUTHOR_NAME,
  lastname: AUTHOR_LASTNAME,
});

module.exports = { getInfo };
