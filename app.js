const createServer = require("./server");
const app = createServer();

const port = process.env.PORT || 3000;
const host = "0.0.0.0";

app.listen(port, host, () => console.log(`Listening on port: ${port}`));
