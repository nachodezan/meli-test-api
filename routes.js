const router = require("express").Router();
const items = require("./controllers/products");

router.use("/items", items);

module.exports = router;
