const {
  searchItems,
  getProductDescription,
} = require("../../services/products");

describe("test service product", () => {
  test("should return an array of products", async () => {
    const products = await searchItems("Apple Macbook");
    expect(typeof products).toBe("object");
    expect(products).toHaveProperty("author");
    expect(products).toHaveProperty("categories");
    expect(products).toHaveProperty("items");
  });

  test("should return an item description", async () => {
    const detail = await getProductDescription("MLA886707517");
    expect(typeof detail).toBe("object");
    expect(detail).toHaveProperty("author");
    expect(detail).toHaveProperty("item");
  });
});
