const supertest = require("supertest");
const createServer = require("../server");
const app = createServer();

describe("GET /items", () => {
  test("Empty query search", async () => {
    const error = {
      status: "402",
      message: "Mandatory Parameter Empty.",
      data: "Mandatory Parameter Empty.",
    };

    await supertest(app)
      .get("/api/items?q=")
      .expect(402)
      .then((response) => {
        expect(typeof response.body).toBe("object");
        expect(response.body).toHaveProperty("status", error.status);
        expect(response.body).toHaveProperty("message", error.message);
        expect(response.body).toHaveProperty("data", error.data);
      });
  });

  test("Empty query param id item", async () => {
    const error = {
      status: "402",
      message: "Mandatory Parameter Empty.",
      data: "Mandatory Parameter Empty.",
    };

    await supertest(app)
      .get("/api/items/")
      .expect(402)
      .then((response) => {
        expect(typeof response.body).toBe("object");
        expect(response.body).toHaveProperty("status", error.status);
        expect(response.body).toHaveProperty("message", error.message);
        expect(response.body).toHaveProperty("data", error.data);
      });
  });

  test("Structure Response", async () => {
    await supertest(app)
      .get("/api/items?q=Apple Ipod")
      .expect(200)
      .then((response) => {
        expect(Array.isArray(response.body.items)).toBeTruthy();
        expect(Array.isArray(response.body.categories)).toBeTruthy();
        expect(response.body.items.length).toBeGreaterThanOrEqual(1);
      });
  });
});
