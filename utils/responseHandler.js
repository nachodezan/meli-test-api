const { ERROR_STATUS_ARRAY } = require("../config/constants");

const errorResponse = (status) => {
  return (
    ERROR_STATUS_ARRAY.find((v) => v.status == status) || {
      error: "There must be an error",
    }
  );
};

module.exports = { errorResponse };
