const api = require("../api");
const author = require("../models/author");
const {
  FILTER_CATEGORY_ID,
  PRICE_DECIMALS,
  LIMIT_PRODUCTS,
} = require("../config/constants");

const getCategory = (categoryId) => api.get(`/categories/${categoryId}`);

const getSearchItems = (searchParams, limit = LIMIT_PRODUCTS) =>
  api.get(`/sites/MLA/search?q=${searchParams}&limit=${limit}`);

const getItemInfo = (itemId) => api.get(`/items/${itemId}`);

const getDescription = (itemId) => api.get(`/items/${itemId}/description`);

const getFormattedItems = (results) => {
  return results.map(
    ({
      id,
      title,
      currency_id: currency,
      price,
      thumbnail: picture,
      condition,
      shipping,
      address,
    }) => ({
      id,
      title,
      price: {
        currency,
        amount: price,
        decimals: PRICE_DECIMALS,
      },
      picture,
      condition,
      free_shipping: shipping.free_shipping,
      address: address.state_name,
    })
  );
};

const getCategories = (filters) => {
  if (!filters.length) return [];
  const { values } = filters.find((filter) => filter.id === FILTER_CATEGORY_ID);
  const categories =
    values && values.length > 0
      ? values[0].path_from_root.map((category) => category.name)
      : [];
  return categories;
};

const getFormattedDescription = ({ info, description, categoriesData }) => ({
  id: info.id,
  title: info.title,
  price: {
    currency: info.currency_id,
    amount: info.price,
    decimals: PRICE_DECIMALS,
  },
  picture: info.pictures[0].secure_url,
  condition: info.condition,
  free_shipping: info.shipping.free_shipping,
  sold_quantity: info.sold_quantity,
  description,
  categories:
    categoriesData.length > 0
      ? categoriesData.map(({ name }) => name)
      : categoriesData,
});

const searchItems = async (q) => {
  const authorData = author.getInfo();
  const { results, filters } = await getSearchItems(q);
  const items = getFormattedItems(results);
  const categories = getCategories(filters);

  return { author: authorData, categories, items };
};

const getProductDescription = async (id) => {
  const authorData = author.getInfo();
  const info = await getItemInfo(id);
  const { plain_text: description } = await getDescription(id);
  const { path_from_root: categoriesData } = await getCategory(
    info.category_id
  );
  const item = getFormattedDescription({ info, description, categoriesData });
  return { author: authorData, item };
};

module.exports = { searchItems, getProductDescription };
