const fetch = require("node-fetch");
const { API_URL } = require("./config/constants");

const checkResponseStatus = async (res) => {
  if (res.ok) {
    return res;
  } else {
    const error = await res.json();
    throw new Error(error.message);
  }
};

const get = (endpoint) =>
  fetch(`${API_URL}${endpoint}`)
    .then(checkResponseStatus)
    .then((res) => res.json());

module.exports = { get };
