const router = require("express").Router();
const service = require("../services/products");
const responseHandler = require("../utils/responseHandler");

const {
  validateParam,
  validateQueryParam,
} = require("../middlewares/products");

const fetchItemsSearch = async (req, res) => {
  const { q } = req.query;
  try {
    const result = await service.searchItems(q);
    return res.json(result);
  } catch (e) {
    console.error(e.message);
    return res.status(501).json(responseHandler.errorResponse(501));
  }
};

const fetchItemDescription = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await service.getProductDescription(id);
    return res.json(result);
  } catch (e) {
    console.error(e.message);
    return res.status(501).json(responseHandler.errorResponse(501));
  }
};

router.get("", validateQueryParam, fetchItemsSearch);
router.get("/:id", validateParam, fetchItemDescription);

module.exports = router;
